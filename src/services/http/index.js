import axios from "axios";


const instance = axios.create({
    timeout: 1000,
});

const resquestGET = (url,  params= null) => {
    return  instance.get(url, params).then(Response => Response)
}

const requersPost = (url,  data) => {
    return  instance.post(url, data).then(Response => Response)
}

export{resquestGET, requersPost}; 